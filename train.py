import click
import pytorch_lightning as pl
import yaml
from lightning_module import LightningModule
from pytorch_lightning.callbacks import LearningRateMonitor, ModelCheckpoint, early_stopping
from pytorch_lightning.loggers import TensorBoardLogger
import ssl

ssl._create_default_https_context = ssl._create_stdlib_context


@click.command()
@click.argument("config_path", type=click.Path(exists=True))
def main(config_path):
    with open(config_path, "r") as f:
        config = yaml.load(f, Loader=yaml.FullLoader)

    model = LightningModule(config)

    early_stop_callback = early_stopping.EarlyStopping(**config["early_stop"])
    checkpoint_callback = ModelCheckpoint(**config["model_checkpoint"])
    lr_monitor = LearningRateMonitor(logging_interval="epoch")

    tb_logger = TensorBoardLogger(
        save_dir="lightning_logs", name=config["experiment"], default_hp_metric=False
    )

    trainer = pl.Trainer(
        accelerator="gpu",
        devices=config["devices"],
        strategy=config["strategy"],
        max_epochs=config["epochs"],
        precision=config["precision"],
        callbacks=[checkpoint_callback, lr_monitor, early_stop_callback],
        logger=tb_logger,
        check_val_every_n_epoch=config["check_val_every_n_epoch"],
    )

    trainer.fit(model)


if __name__ == "__main__":
    main()
