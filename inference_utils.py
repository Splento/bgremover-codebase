import os

import albumentations as albu
import cv2
import matplotlib.pyplot as plt
import numpy as np
import segmentation_models_pytorch as smp
import torch
import yaml
from torch import from_numpy, sigmoid


def imshow(img, convert=False):
    img_show = cv2.cvtColor(img, cv2.COLOR_BGR2RGB) if convert else img
    plt.imshow(img_show)
    plt.axis("off")
    plt.show()


def get_inference_albu_transforms(size=[256, 256]):

    transforms = [
        albu.Resize(
            height=size[0],
            width=size[1],
            always_apply=True,
            interpolation=cv2.INTER_AREA,
            p=1,
        ),
        albu.Normalize(
            mean=[0.449, 0.449, 0.449],
            std=[0.226, 0.226, 0.226],
            always_apply=True,
            p=1,
        ),
    ]
    return transforms


def init_model(
    models_dir: str, version: int = -1, device: torch.device = torch.device("cuda:0")
):
    """Find a checkpoint. Init a Unet++ model.

    Args:
        version: model version. -1 to use the latest model in models_dir
        models_dir: directory with trained model (w/ logs, checkpoints, ...)
        device: torch.device
    Returns:
        inited model, model input size
    """

    # Find a checkpoint
    latest_version = max(
        [
            int(dir_name.split("version_")[1])
            for dir_name in os.listdir(models_dir)
            if dir_name.startswith("version_")
        ]
    )
    version = latest_version if version == -1 else version

    version_name = f"version_{version}"
    version_dir = os.path.join(models_dir, version_name)

    model_jit_name = 'model-jit-cpu.pt' if device == torch.device("cpu") else 'model-jit-gpu.pt'
    model_jit_path = os.path.join(version_dir, model_jit_name)
    config_path = os.path.join(models_dir, version_name, "hparams.yaml")

    # Load a config file
    with open(config_path, "r") as f:
        config = yaml.load(f, Loader=yaml.FullLoader)
    size = config["dataset"]["val"]["size"]
    
    # load a traced model
    if os.path.isfile(model_jit_path):
        print(f'Loading a traced model from: {model_jit_path}')
        model = torch.jit.load(model_jit_path).eval()
    # init a model + load weights from checkpoint + trace the model
    else:
        # Get the checkpoint path
        ckpt_dir = os.path.join(version_dir, "checkpoints")
        ckpt_name = [
            filename for filename in os.listdir(ckpt_dir) if filename.endswith(".ckpt")
        ][0]
        ckpt_path = os.path.join(ckpt_dir, ckpt_name)

        # Init a Unet++ model
        model = smp.UnetPlusPlus(**config["model"]["params"])

        # Load weights and fix state dict
        state_dict = torch.load(ckpt_path)["state_dict"]
        fixed_state_dict = {
            key.replace("net.", ""): value for key, value in state_dict.items()
        }
        model.load_state_dict(fixed_state_dict)
        model.to(device=device).eval()

        # Trace the model
        model_jit = torch.jit.trace(model, torch.rand((1, 3, size[0], size[1])).to(device=device))
        torch.jit.save(model_jit, model_jit_path)
        print(f'Traced model has been saved to: {model_jit_path}')

    return model, size


def predict_frame(
    model,
    transforms,
    device=torch.device("cuda:0"),
    img_input=None,
    img_path=None,
    max_contour_thr=0.25
):
    if (img_input is not None) and (img_path is None):
        image_raw = img_input.copy()
    elif (img_path is not None) and (img_input is None):
        image_raw = cv2.imread(img_path)
    else:
        raise ValueError("Please provide either img_path or image_raw")

    image_raw = cv2.cvtColor(image_raw, cv2.COLOR_BGR2RGB)

    h_raw, w_raw, _ = image_raw.shape
    pipeline_full = albu.Compose(transforms, p=1)
    image = pipeline_full(image=image_raw)["image"]

    model_input = (
        from_numpy(np.transpose(image, (2, 0, 1)))
        .float()
        .unsqueeze(0)
        .to(device=device)
    )
    predict = model(model_input)
    predict = sigmoid(predict)

    predict = predict.squeeze(0).squeeze(0).detach().cpu().numpy()

    predict_upscale = cv2.resize(predict, (w_raw, h_raw))  # upscale to input size

    # preserve the mask within the largest contour only
    # to eliminate outliers
    max_contour_mask = np.zeros_like(predict_upscale)

    _, thresh = cv2.threshold((255 * predict_upscale).astype(np.uint8), max_contour_thr * 255, 255, 0)
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    max_contour = max(contours, key = cv2.contourArea)
    cv2.drawContours(max_contour_mask, [max_contour], -1, 1, -1)

    predict_upscale *= max_contour_mask

    return image_raw, predict_upscale


def get_min_max_coords(pred):
    argwhere = np.argwhere(pred)

    y_min, y_max = np.min(argwhere[:, 0]), np.max(argwhere[:, 0])
    x_min, x_max = np.min(argwhere[:, 1]), np.max(argwhere[:, 1])
    
    return y_min, y_max, x_min, x_max


def make_png(image, pred, cvt=False, vis=False):

    result = 0. * np.zeros_like(image)
    result += image * np.expand_dims(pred, -1)

    # Convert to RGB
    if cvt:
        result = cv2.cvtColor(result.astype(np.uint8), cv2.COLOR_BGR2RGB)

    result = cv2.cvtColor(result.astype(np.uint8), cv2.COLOR_BGR2BGRA)
    
    # Add transparency channel
    result[:,:,-1] = (255 * pred).astype(np.uint8)

    # Visualise
    if vis:
        imshow(result, True)

    y_min, y_max, x_min, x_max = get_min_max_coords(pred)
    result_crop = result[y_min: y_max + 1, x_min: x_max + 1]
    
    return result, result_crop


def make_greenscreen(image, pred, cvt=False, vis=False):
    # Create a green-screen version of the predictions
    pred_rgb = np.moveaxis([pred for _ in range(3)], 0, -1)   # to RGB
    
    gs_frame = [0, 177, 64] * np.ones_like(image)
    gs_frame = gs_frame * (1 - pred_rgb) + image * pred_rgb
    gs_frame = gs_frame.astype(np.uint8)

    # Stack input image and green-screen predictions
    result = np.hstack([image, gs_frame]).astype(np.uint8)

    # Visualise
    if vis:
        imshow(result, True)

    # Convert to RGB
    if cvt:
        result = cv2.cvtColor(result, cv2.COLOR_BGR2RGB)
    
    return result