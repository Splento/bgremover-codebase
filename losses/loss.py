import torch
import torch.nn.functional as F

# Loss from https://github.com/plemeri/InSPyReNet

def bce_loss(pred, mask, reduction='none'):
    bce = F.binary_cross_entropy_with_logits(pred, mask, reduction=reduction)
    return bce

def weighted_bce_loss(pred, mask, reduction='mean'):
    """
    Source: https://github.com/plemeri/InSPyReNet/blob/548d07ac6a0a40333602eb2dc745b4be64df474e/lib/optim/losses.py
    """
    weight = 1 + 5 * torch.abs(F.avg_pool2d(mask, kernel_size=31, stride=1, padding=15) - mask)
    weight = weight.flatten()
    
    bce = weight * bce_loss(pred, mask, reduction='none').flatten()
    
    if reduction == 'mean':
        bce = bce.mean()
    
    return bce