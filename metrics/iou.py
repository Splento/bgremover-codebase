import torch


def iou(predict, ground_truth, eps=1e-7, threshold=0.5, apply_sigmoid=True):
    if apply_sigmoid:
        predict = torch.sigmoid(predict)

    predict = predict > threshold
    ground_truth = ground_truth.type(predict.dtype)

    intersection = torch.sum(ground_truth * predict)
    union = torch.sum(ground_truth) + torch.sum(predict) - intersection

    return (intersection + eps) / (union + eps)
