### Getting started

* ```conda create -n food python=3.8 -y```
* ```conda activate food```
* ```pip3 install -r requirements.txt```

<br>

### Data Preparation
1. Download dataset into `../dataset`
2. If you have additional labeled data you can move it into `../dataset/images` and `../dataset/masks` respectively. Make sure you masks are binary 2d numpy arrays of type `float64`.
3. Update `split/split.json` file by adding new image names into `train` subset.


### Model training
1. Make sure your conda env is activated and GPUs are available
2. Update config file `configs/training_config.yaml` if needed 
3. Run `python3 train.py configs/training_config.yaml`
4. To track the model training progres, run `tensorboard --logdir lightning_logs/`
5. Model training logs, config file and checkpoint with model's weights will be saved into `lightning_logs/food-segm/version_*`


### Model inference
To run model inference, use `inference.ipynb` jupyter notebook.
<br>

Parameters you can specify are the following:
* `VERSION` - the latest (or any specific) version of the model, check `lightning_logs/food-segm/version_*`
* `device` - either `torch.device('cuda')` or `torch.device('cpu')` to run the inference on GPU or CPU
* `raw_frames_folder` - path to a folder with images you want to run inference for

Once you run inference, the model will be traced to a JIT and saved to `lightning_logs/food-segm/version_*/` folder. According to the device you chose it will be saved as `model-jit-gpu.pt` or `model-jit-cpu.pt`.
