import os

import albumentations as albu
import cv2
import numpy as np
import torch
from torch.utils.data import Dataset

from .dataset_utils import generate_shadows


class FoodDataset(Dataset):
    def __init__(
        self,
        files: list,
        data_path: str,
        mode: str = "train",
        size: list = 256,
        insert_dataset_paths: str = None,
    ):
        self.files = files
        self.mode = mode
        self.size = size
        self.data_path = data_path
        self.insert_dataset_paths = insert_dataset_paths

    def __len__(self):
        return len(self.files)

    def __getitem__(self, index):
        img_name = self.files[index]
        mask_name = os.path.splitext(img_name)[0] + ".npy"

        image_path = os.path.join(self.data_path, "images", img_name)
        mask_path = os.path.join(self.data_path, "masks", mask_name)

        image = cv2.cvtColor(cv2.imread(image_path), cv2.COLOR_BGR2RGB)
        mask = np.load(mask_path)

        # Generate shadows on train
        if self.mode == "train":
            if np.random.rand() < 0.25:
                image = generate_shadows(image, mask, intensity_range=[0.7, 0.99])

        self.define_transform()

        pipeline = albu.Compose(self.transforms, p=1)
        augmented = pipeline(image=image, mask=mask)

        image = augmented["image"]
        mask = augmented["mask"]

        image = torch.from_numpy(np.transpose(image, (2, 0, 1))).float()
        mask = torch.from_numpy(np.round(mask)).float().unsqueeze(0)

        return image, mask

    def define_transform(self):
        ssr_transform = albu.ShiftScaleRotate(
            shift_limit=0.05,
            scale_limit=0.1,
            rotate_limit=20,
            border_mode=0,
            p=0.85,
        )

        if self.mode == "train":
            self.transforms = [
                albu.RandomBrightnessContrast(
                    contrast_limit=0.3, brightness_limit=0.3, p=0.75
                ),
                albu.HorizontalFlip(p=0.5),
                albu.VerticalFlip(p=0.5),
                albu.Transpose(p=0.5),
                ssr_transform,
                albu.OneOf(
                    [
                        albu.RGBShift(
                            r_shift_limit=50, g_shift_limit=50, b_shift_limit=50, p=0.5
                        ),
                        albu.HueSaturationValue(
                            hue_shift_limit=50,
                            sat_shift_limit=50,
                            val_shift_limit=50,
                            p=0.5,
                        ),
                    ],
                    p=0.75,
                ),
                albu.GaussNoise(p=0.25),
                albu.GaussianBlur(blur_limit=(3, 7), p=0.5),
                albu.Resize(
                    height=self.size[0],
                    width=self.size[1],
                    always_apply=True,
                    interpolation=cv2.INTER_AREA,
                    p=1,
                ),
                albu.Normalize(
                    mean=[0.449, 0.449, 0.449],
                    std=[0.226, 0.226, 0.226],
                    always_apply=True,
                    p=1,
                ),
            ]
        if self.mode == "val":
            self.transforms = [
                albu.Resize(
                    height=self.size[0],
                    width=self.size[1],
                    always_apply=True,
                    interpolation=cv2.INTER_AREA,
                    p=1,
                ),
                albu.Normalize(
                    mean=[0.449, 0.449, 0.449],
                    std=[0.226, 0.226, 0.226],
                    always_apply=True,
                    p=1,
                ),
            ]
