import cv2
import random
import numpy as np


def _to_3_channel(mask):
    return np.repeat(mask[..., np.newaxis], 3, axis=2)

def _equidiameter(mask):
    area = np.sum(mask)
    equi_diameter = np.sqrt(4 * area / np.pi)
    return equi_diameter

def shift_image(mask, dx, dy):
    X = np.copy(mask)
    X = np.roll(X, dy, axis=0)
    X = np.roll(X, dx, axis=1)
    if dy>0:
        X[:dy, :] = 0
    elif dy<0:
        X[dy:, :] = 0
    if dx>0:
        X[:, :dx] = 0
    elif dx<0:
        X[:, dx:] = 0
    return X

def generate_mask_aware_params(
        mask,
        intensity_range: list[float] = [0.5, 0.95],
        shift_range: list[float] = [0.05, 0.1],
        erosion_kernel_scale_range: list[float] = [0.02, 0.1],
        blur_kernel_scale_range: list[float] = [1., 2.]
        ):
    """
    intensity_range: Shadow intensity. [min intensity, max intensity]. 0.0 is for no shadow, 1.0 is for darkest shadow
    shift_range: Shadow shift for one of the axises in percentages of equivalent diameter. [min %, max %].
                    shift in range [min %, max %] for one of the axises; shift in range [0, max %] for the second axis
    erosion_kernel_scale_range: kernel for mask erosion in percentages of equivalent diameter. [min %, max %]
    blur_kernel_scale_range: kernel for GaussianBlur. [min kernel_scale, max kernel_scale].
                    The actual kernel size will be equal to kernel_scale * shift
    """
    # Intensity
    intensity = np.random.uniform(*intensity_range)

    # Shift
    equi_diameter = _equidiameter(mask)
    shift_dx_dy = np.array([
        np.random.uniform(*shift_range),
        np.random.uniform(0, shift_range[1])
    ], float)
    shift_dx_dy = shift_dx_dy * np.random.choice([-1, 1], 2)   # allow negative shifts
    shift_dx_dy = np.round(equi_diameter * shift_dx_dy).astype(int)
    dx, dy = np.random.permutation(shift_dx_dy)

    # Erosion kernel
    kernel_erosion = np.random.uniform(*erosion_kernel_scale_range) * equi_diameter
    kernel_erosion = int(2 * (kernel_erosion // 2) + 1)   # nearest odd value

    # Blur kernel 
    kernel_blur = np.random.uniform(*blur_kernel_scale_range) * np.max(np.abs(shift_dx_dy))
    kernel_blur = int(2 * (kernel_blur // 2) + 1)   # nearest odd value

    return intensity, [dx, dy], kernel_erosion, kernel_blur

def generate_shadows(img, mask, intensity_range: list[float] = [0.5, 0.95]):

    intensity, shift, kernel_erosion, kernel_blur = generate_mask_aware_params(mask, intensity_range=intensity_range)

    img_wo_obj = img * _to_3_channel(1 - mask)

    # Shift
    shadow = shift_image(mask, *shift)

    # Erosion
    shadow = cv2.erode(shadow, np.ones((kernel_erosion, kernel_erosion), np.uint8)) 

    # Blur
    shadow = cv2.GaussianBlur(shadow, (kernel_blur, kernel_blur), 0)

    # Add shadow
    img_wo_obj_w_shadow = img_wo_obj * _to_3_channel(1 - intensity * shadow)

    # Combine
    result_image = img_wo_obj_w_shadow * _to_3_channel(1 - mask) + img * _to_3_channel(mask)
    
    return result_image.astype("uint8")