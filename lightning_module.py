import itertools
import json

import cv2
import numpy as np
import pytorch_lightning as pl
import segmentation_models_pytorch as smp
import torch
import torchvision
from dataset.dataset import FoodDataset
from losses.loss import weighted_bce_loss
from metrics.iou import iou
from torch.utils.data import DataLoader


class LightningModule(pl.LightningModule):
    def __init__(self, config):
        super().__init__()
        self.hparams.update(config)

        self.net = smp.UnetPlusPlus(**config["model"]["params"])

        self.loss = weighted_bce_loss

        self.metric = iou

        self.files = json.load(open(config["split"]))
        data_path = config["data_path"]

        val_subsets = config["val_subsets"]
        train_subsets = config["train_subsets"]

        self.train_files = list(
            itertools.chain(*[self.files[subset] for subset in train_subsets])
        )
        self.val_files = list(
            itertools.chain(*[self.files[subset] for subset in val_subsets])
        )

        self.train_data = FoodDataset(
            files=self.train_files,
            data_path=data_path,
            mode="train",
            **config["dataset"]["train"]
        )
        self.val_data = FoodDataset(
            files=self.val_files,
            data_path=data_path,
            mode="val",
            **config["dataset"]["val"]
        )

        self.validation_step_outputs = []
        self.validation_step_vis = ()
        self.save_hyperparameters()

    def forward(self, x):
        return self.net(x)

    def training_step(self, batch):
        image, mask = batch
        predict = self.forward(image)
        train_loss = self.loss(predict, mask)

        self.log("train_loss", train_loss, on_step=False, on_epoch=True, prog_bar=True, sync_dist=True)

        return {
            "loss": train_loss,
        }

    def validation_step(self, batch, batch_idx):
        image, mask = batch
        predict = self.forward(image)

        val_loss = self.loss(predict, mask)

        val_iou = self.metric(predict, mask)

        self.log("val_loss", val_loss, on_step=False, on_epoch=True, prog_bar=True, sync_dist=True)

        self.log("val_iou", val_iou, on_step=False, on_epoch=True, prog_bar=True, sync_dist=True)

        if batch_idx == 0:
            self.validation_step_vis = (image, mask, predict)
            
        outputs = {
            "val_loss": val_loss,
            "val_iou": val_iou,
        }

        self.validation_step_outputs.append(outputs)

        return outputs

    def on_validation_epoch_end(self):
        val_loss = np.mean([output["val_loss"].detach().cpu() for output in self.validation_step_outputs])
        val_iou = np.mean([output["val_iou"].detach().cpu() for output in self.validation_step_outputs])

        self.save_predictions()

        return {
            "val_loss": val_loss,
            "val_iou": val_iou,
        }

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.net.parameters(), lr=self.hparams["lr"])
        scheduler = {
            "scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(
                optimizer, **self.hparams["scheduler"]["params"]
            ),
            **self.hparams["scheduler"]["additional_params"],
        }
        return [optimizer], [scheduler]

    def train_dataloader(self):
        return DataLoader(
            self.train_data,
            batch_size=self.hparams.batch_size,
            shuffle=True,
            drop_last=True,
            num_workers=4,
        )

    def val_dataloader(self):
        return DataLoader(
            self.val_data,
            batch_size=self.hparams.batch_size,
            shuffle=False,
            drop_last=False,
            num_workers=4,
        )

    def save_predictions(self):  
        image, gt, predict = self.validation_step_vis

        image_frame = image[-1].detach().cpu().numpy()
        image_frame = (255 * (0.226 * image_frame + 0.449)).astype(
            np.uint8
        )  # [-1, 1] -> [0, 255]
        image_frame = np.transpose(image_frame, (1, 2, 0))
        image_frame = (cv2.cvtColor(image_frame, cv2.COLOR_BGR2GRAY) / 255).astype(
            float
        )

        # ground truth
        gt_frame = gt[-1, 0].squeeze(0).detach().cpu().numpy()

        # predict
        predict_frame = torch.sigmoid(predict[-1, 0])  # apply sigmoid
        predict_frame = predict_frame.squeeze(0).detach().cpu().numpy()

        # stack and save
        stacked_frame = np.hstack([image_frame, gt_frame, predict_frame])

        grid = torchvision.utils.make_grid(torch.from_numpy(stacked_frame))
        self.logger.experiment.add_image("images", grid, self.current_epoch)
